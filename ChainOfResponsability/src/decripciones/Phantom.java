package decripciones;

public class Phantom implements IConcesionario {

	private IConcesionario sucesor = new SedanRollsRoyce();
	private final int descripcionProfunda = 4;
	@Override
	public void getDescripcion(int tipoDescripcion) {

		if(tipoDescripcion == descripcionProfunda) {
			System.out.println("----------------------------------------");
			System.out.println("//  Sedan  -  RollsRoyce  -  Phantom  //");
			System.out.println("//         Alta eficacia               //");
			System.out.println("//         Auto increible             //");
			System.out.println("----------------------------------------");
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}
}
