package decripciones;

public class CamionetaPorshe implements IConcesionario {

	private IConcesionario sucesor = new Porshe();
	private final int descripcionMedia = 3;
	@Override
	public void getDescripcion(int tipoDescripcion) {
		
		if(tipoDescripcion == descripcionMedia) {
		
		System.out.println("---------------------------");
		System.out.println("//  Porshe                   //");
		System.out.println("//  Comodidad y confort      //");
		System.out.println("//  Modelos muy prestigiosos //");
		System.out.println("---------------------------");
		} else {
			sucesor.getDescripcion(tipoDescripcion);
		}
	}

	
}


